class AddProponentToContact < ActiveRecord::Migration[5.0]
  def change
    add_reference :contacts, :proponent, foreign_key: true
  end
end
