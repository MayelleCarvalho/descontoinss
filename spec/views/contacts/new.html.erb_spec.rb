require 'rails_helper'

RSpec.describe 'contacts/new', type: :view do
  before(:each) do
    assign(:contact, Contact.new(
                       kind: 'MyString',
                       value: 'MyString'
                     ))
  end

  it 'renders new contact form' do
    render

    assert_select 'form[action=?][method=?]', contacts_path, 'post' do
      assert_select 'input#contact_kind[name=?]', 'contact[kind]'

      assert_select 'input#contact_value[name=?]', 'contact[value]'
    end
  end
end
