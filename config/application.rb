require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Descontoinss
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers

    config.active_job.queue_adapter = :sidekiq
    # -- all .rb files in that directory are automatically loaded.
  end
end

Time::DATE_FORMATS[:default] = '%d/%m/%Y %H:%M'
Date::DATE_FORMATS[:default] = '%d/%m/%Y'
