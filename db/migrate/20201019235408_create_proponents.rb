class CreateProponents < ActiveRecord::Migration[5.0]
  def change
    create_table :proponents do |t|
      t.string :name
      t.string :cpf
      t.datetime :born_at
      t.references :address, foreign_key: true
      t.decimal :salary

      t.timestamps
    end
  end
end
