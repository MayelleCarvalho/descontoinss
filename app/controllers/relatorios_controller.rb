class RelatoriosController < ApplicationController
  def search
    @proponents = if (params[:q] != '') && params[:q]
                    Proponent.where('salary = ?', params[:q])
                  else
                    # @proponents = Proponent.where.not(salary: [1045, 3000])
                    Proponent.all

                    # @proponents = Proponent.select('salary AS faixa_salarial_1').where(salary: 1045..3000)
                  end
  end

  def show
    @proponents_faixa_1 = Proponent.where(salary: 0..1045.00)
    @proponents_faixa_2 = Proponent.where(salary: 1045.01..2089.6)
    @proponents_faixa_3 = Proponent.where(salary: 2089.61..3134.4)
    @proponents_faixa_4 = Proponent.where(salary: 3134.41..6101.06)

    @quantidade_faixa_1 = Proponent.where(salary: 0..1045.00).count
    @quantidade_faixa_2 = Proponent.where(salary: 1045.01..2089.6).count
    @quantidade_faixa_3 = Proponent.where(salary: 2089.61..3134.4).count
    @quantidade_faixa_4 = Proponent.where(salary: 3134.41..6101.06).count

    @total_desconto_faixa_1 = Proponent.where(salary: 0..1045.00).sum(:discount)
    @total_desconto_faixa_2 = Proponent.where(salary: 1045.01..2089.6).sum(:discount)
    @total_desconto_faixa_3 = Proponent.where(salary: 2089.61..3134.4).sum(:discount)
    @total_desconto_faixa_4 = Proponent.where(salary: 3134.41..6101.06).sum(:discount)
  end
end
