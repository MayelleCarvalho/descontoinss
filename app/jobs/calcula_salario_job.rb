class CalculaSalarioJob < ApplicationJob
  queue_as :salarys

  def perform(id_proponent)
    sleep 10

    @proponent = Proponent.find(id_proponent)
    @proponent.amount_salary = @proponent.salary - @proponent.discount
    @proponent.save
  end
end
