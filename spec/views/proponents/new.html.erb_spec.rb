require 'rails_helper'

RSpec.describe 'proponents/new', type: :view do
  before(:each) do
    assign(:proponent, Proponent.new(
                         name: 'MyString',
                         cpf: 'MyString',
                         address: nil,
                         salary: '9.99'
                       ))
  end

  it 'renders new proponent form' do
    render

    assert_select 'form[action=?][method=?]', proponents_path, 'post' do
      assert_select 'input#proponent_name[name=?]', 'proponent[name]'

      assert_select 'input#proponent_cpf[name=?]', 'proponent[cpf]'

      assert_select 'input#proponent_address_id[name=?]', 'proponent[address_id]'

      assert_select 'input#proponent_salary[name=?]', 'proponent[salary]'
    end
  end
end
