require 'rails_helper'

RSpec.describe 'contacts/show', type: :view do
  before(:each) do
    @contact = assign(:contact, Contact.create!(
                                  kind: 'Kind',
                                  value: 'Value'
                                ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Kind/)
    expect(rendered).to match(/Value/)
  end
end
