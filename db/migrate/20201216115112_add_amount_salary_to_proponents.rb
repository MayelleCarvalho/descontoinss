class AddAmountSalaryToProponents < ActiveRecord::Migration[5.0]
  def change
    add_column :proponents, :amount_salary, :decimal, precision: 10, scale: 2, default: 0.00
  end
end
