class Proponent < ApplicationRecord
  belongs_to :address
  has_many :contacts

  validates :name, :cpf, :born_at, :salary, presence: true
  validates :name, length: { minimum: 12, maximum: 70 }
  validates :cpf, uniqueness: true
  validate :validator_cpf

  accepts_nested_attributes_for :contacts, allow_destroy: true
  accepts_nested_attributes_for :address, allow_destroy: true

  def validator_cpf
    errors.add(:cpf, 'O CPF informado é inválido!') unless CPF.valid?(cpf)
  end
end
