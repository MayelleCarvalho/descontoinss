require 'rails_helper'

RSpec.describe 'addresses/index', type: :view do
  before(:each) do
    assign(:addresses, [
             Address.create!(
               street: 'Street',
               number: 'Number',
               district: 'District',
               city: 'City',
               state: 'State',
               cep: 'Cep'
             ),
             Address.create!(
               street: 'Street',
               number: 'Number',
               district: 'District',
               city: 'City',
               state: 'State',
               cep: 'Cep'
             )
           ])
  end

  it 'renders a list of addresses' do
    render
    assert_select 'tr>td', text: 'Street'.to_s, count: 2
    assert_select 'tr>td', text: 'Number'.to_s, count: 2
    assert_select 'tr>td', text: 'District'.to_s, count: 2
    assert_select 'tr>td', text: 'City'.to_s, count: 2
    assert_select 'tr>td', text: 'State'.to_s, count: 2
    assert_select 'tr>td', text: 'Cep'.to_s, count: 2
  end
end
