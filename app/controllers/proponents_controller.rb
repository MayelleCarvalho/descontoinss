class ProponentsController < ApplicationController
  before_action :set_proponent, only: %i[show edit update destroy]

  # GET /proponents
  # GET /proponents.json
  def index
    @proponents = Proponent.all.order('id').page(params[:page]).per(5)
  end

  # GET /proponents/1
  # GET /proponents/1.json
  def show; end

  # GET /proponents/new
  def new
    @proponent = Proponent.new
    @proponent.build_address
  end

  # GET /proponents/1/edit
  def edit; end

  # POST /proponents
  # POST /proponents.json
  def create
    @proponent = Proponent.new(proponent_params)

    respond_to do |format|
      if @proponent.save
        format.html { redirect_to proponent_path, notice: 'Proponent was successfully created.' }
        # format.js
      else
        format.html { render :new }
        # format.js
      end
    end
  end

  # PATCH/PUT /proponents/1
  # PATCH/PUT /proponents/1.json
  def update
    respond_to do |format|
      if @proponent.update(proponent_params)
        format.html { redirect_to @proponent, notice: 'Proponent was successfully updated.' }
        format.js
      else
        format.html { render :edit }
        format.js
      end
    end
  end

  # DELETE /proponents/1
  # DELETE /proponents/1.json
  def destroy
    @proponent.destroy
    respond_to do |format|
      format.html { redirect_to proponents_url, notice: 'Proponent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_proponent
    @proponent = Proponent.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def proponent_params
    params.require(:proponent).permit(:name, :cpf, :born_at, :address_id, :salary, :discount,
                                      contacts_attributes: %i[
                                        id kind value _destroy
                                      ],
                                      address_attributes: %i[
                                        id street number district city state cep _destroy
                                      ])
  end
end
