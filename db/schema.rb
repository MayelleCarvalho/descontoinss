# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_201_216_115_112) do
  create_table 'addresses', force: :cascade do |t|
    t.string   'street'
    t.string   'number'
    t.string   'district'
    t.string   'city'
    t.string   'state'
    t.string   'cep'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'contacts', force: :cascade do |t|
    t.string   'kind'
    t.string   'value'
    t.datetime 'created_at',   null: false
    t.datetime 'updated_at',   null: false
    t.integer  'proponent_id'
    t.index ['proponent_id'], name: 'index_contacts_on_proponent_id'
  end

  create_table 'proponents', force: :cascade do |t|
    t.string   'name'
    t.string   'cpf'
    t.datetime 'born_at'
    t.integer  'address_id'
    t.decimal  'salary'
    t.datetime 'created_at',                                             null: false
    t.datetime 'updated_at',                                             null: false
    t.decimal  'discount'
    t.decimal  'amount_salary', precision: 10, scale: 2, default: '0.0'
    t.index ['address_id'], name: 'index_proponents_on_address_id'
  end

  create_table 'users', force: :cascade do |t|
    t.string   'email',                  default: '', null: false
    t.string   'encrypted_password',     default: '', null: false
    t.string   'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.datetime 'created_at',                          null: false
    t.datetime 'updated_at',                          null: false
    t.index ['email'], name: 'index_users_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_users_on_reset_password_token', unique: true
  end
end
