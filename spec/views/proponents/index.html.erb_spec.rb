require 'rails_helper'

RSpec.describe 'proponents/index', type: :view do
  before(:each) do
    assign(:proponents, [
             Proponent.create!(
               name: 'Name',
               cpf: 'Cpf',
               address: nil,
               salary: '9.99'
             ),
             Proponent.create!(
               name: 'Name',
               cpf: 'Cpf',
               address: nil,
               salary: '9.99'
             )
           ])
  end

  it 'renders a list of proponents' do
    render
    assert_select 'tr>td', text: 'Name'.to_s, count: 2
    assert_select 'tr>td', text: 'Cpf'.to_s, count: 2
    assert_select 'tr>td', text: nil.to_s, count: 2
    assert_select 'tr>td', text: '9.99'.to_s, count: 2
  end
end
