class SalarysController < ApplicationController
  def index
    @exibir_btn_calcular = true

    @proponent = Proponent.find(params[:proponent]) if params[:proponent]

    @exibir_btn_calcular = false if params[:exibir]
  end

  def create
    @proponent = Proponent.find(params[:id]) if params[:id]

    CalculaSalarioJob.perform_later @proponent.id

    flash[:notice] = 'Aguarde, estamos calculando o valor do seu salário!'
    redirect_to salarys_index_path(proponent: @proponent.id, exibir: false)
  end
end
