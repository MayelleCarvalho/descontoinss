class AddDiscountToProponent < ActiveRecord::Migration[5.0]
  def change
    add_column :proponents, :discount, :decimal
  end
end
