json.extract! contact, :id, :kind, :value, :created_at, :updated_at
json.url contact_url(contact, format: :json)
