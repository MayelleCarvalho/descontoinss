class Address < ApplicationRecord
  validates :street, :number, :district, :city, :state, :cep, presence: true
  has_many :proponents
end
