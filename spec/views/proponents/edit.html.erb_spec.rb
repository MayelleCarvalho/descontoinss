require 'rails_helper'

RSpec.describe 'proponents/edit', type: :view do
  before(:each) do
    @proponent = assign(:proponent, Proponent.create!(
                                      name: 'MyString',
                                      cpf: 'MyString',
                                      address: nil,
                                      salary: '9.99'
                                    ))
  end

  it 'renders the edit proponent form' do
    render

    assert_select 'form[action=?][method=?]', proponent_path(@proponent), 'post' do
      assert_select 'input#proponent_name[name=?]', 'proponent[name]'

      assert_select 'input#proponent_cpf[name=?]', 'proponent[cpf]'

      assert_select 'input#proponent_address_id[name=?]', 'proponent[address_id]'

      assert_select 'input#proponent_salary[name=?]', 'proponent[salary]'
    end
  end
end
