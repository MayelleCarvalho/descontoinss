require 'sidekiq/web'

Rails.application.routes.draw do
  get 'salarys/index'

  get 'salarys/create'

  get 'relatorios/search', to: 'relatorios#search'
  post 'relatorios/search', to: 'relatorios#search'
  get 'relatorios/show', to: 'relatorios#show'

  devise_for :users
  resources :proponents
  resources :contacts
  resources :addresses
  root 'proponents#index'

  mount Sidekiq::Web => '/sidekiq'
  resources :salarys, only: %i[index create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
