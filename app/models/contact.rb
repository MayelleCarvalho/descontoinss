class Contact < ApplicationRecord
  belongs_to :proponent
  validates :kind, :value, presence: true
  validates :kind, inclusion: { in: %w[PESSOAL REFERENCIAL],
                                message: "%{value} não é válido, é esperado o valor 'PESSOAL' ou 'REFERENCIAL'" }
end
