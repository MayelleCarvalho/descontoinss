require 'rails_helper'

RSpec.describe 'addresses/new', type: :view do
  before(:each) do
    assign(:address, Address.new(
                       street: 'MyString',
                       number: 'MyString',
                       district: 'MyString',
                       city: 'MyString',
                       state: 'MyString',
                       cep: 'MyString'
                     ))
  end

  it 'renders new address form' do
    render

    assert_select 'form[action=?][method=?]', addresses_path, 'post' do
      assert_select 'input#address_street[name=?]', 'address[street]'

      assert_select 'input#address_number[name=?]', 'address[number]'

      assert_select 'input#address_district[name=?]', 'address[district]'

      assert_select 'input#address_city[name=?]', 'address[city]'

      assert_select 'input#address_state[name=?]', 'address[state]'

      assert_select 'input#address_cep[name=?]', 'address[cep]'
    end
  end
end
