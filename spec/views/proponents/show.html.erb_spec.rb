require 'rails_helper'

RSpec.describe 'proponents/show', type: :view do
  before(:each) do
    @proponent = assign(:proponent, Proponent.create!(
                                      name: 'Name',
                                      cpf: 'Cpf',
                                      address: nil,
                                      salary: '9.99'
                                    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Cpf/)
    expect(rendered).to match(//)
    expect(rendered).to match(/9.99/)
  end
end
