require 'rails_helper'

RSpec.describe 'contacts/index', type: :view do
  before(:each) do
    assign(:contacts, [
             Contact.create!(
               kind: 'Kind',
               value: 'Value'
             ),
             Contact.create!(
               kind: 'Kind',
               value: 'Value'
             )
           ])
  end

  it 'renders a list of contacts' do
    render
    assert_select 'tr>td', text: 'Kind'.to_s, count: 2
    assert_select 'tr>td', text: 'Value'.to_s, count: 2
  end
end
