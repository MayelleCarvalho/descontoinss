# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:s.etup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

addresses = [
  { street: 'Rua Estudante José Adalberto', number: '1054', district: 'São José', city: 'Parnaíba', state: 'Piauí',
    cep: '64.218-280' },
  { street: 'Rua Margarita Palmerina de Almeida', number: '852', district: 'Baixa Grande', city: 'Arapiraca',
    state: 'Alagoas', cep: '57.307-160' },
  { street: 'Servidão Cláudio Medina', number: '2358', district: 'Sagrado Coração de Jesus', city: 'Lages',
    state: 'Santa Catarina', cep: '88.508-185' },
  { street: 'Avenida Venezuela', number: '562', district: 'Liberdade', city: 'Boa Vista', state: 'Roraima',
    cep: '69.309-005' },
  { street: 'Rua Palmas', number: '287', district: 'Infraero', city: 'Macapá', state: 'Amapá', cep: '68.908-033' },
  { street: 'Rua Henrique Vivas', number: '840', district: 'Rocha', city: 'São Gonçalo', state: 'Rio de Janeiro',
    cep: '24.422-090' },
  { street: 'Rua Joaquim de Castro', number: '124', district: 'Guarapes', city: 'Natal', state: 'Rio Grande do Norte',
    cep: '59.074-574' },
  { street: 'Rua Vitorino Rodrigues de Monteiro', number: '580', district: 'Ribeirão da Ponte', city: 'Cuiabá',
    state: 'Mato Grosso', cep: '78.040-555' },
  { street: 'Rua Inácio da Catingueira', number: '2383', district: 'Lagoa Azul', city: 'Natal',
    state: 'Rio Grande do Norte', cep: '59.138-450' },
  { street: 'Travessa 2', number: '96', district: 'Vila Goiás', city: 'Araguaína', state: 'Tocantins',
    cep: '77.824-765' }
]

addresses.each do |address|
  Address.create!(address)
end

proponents = [
  { name: 'José dos Santos', cpf: '575.676.130-69', born_at: '21-04-1986', salary: '1200.00', discount: '92.32',
    address_id: 1 },
  { name: 'Francisco Silva', cpf: '978.173.060-95', born_at: '12-05-1996', salary: '1200.00', discount: '92.32',
    address_id: 2 },
  { name: 'Joana Mendes', cpf: '727.565.200-55', born_at: '15-02-1987', salary: '1200.00', discount: '92.32',
    address_id: 3 },
  { name: 'Kassiano Pereira', cpf: '490.625.210-96', born_at: '25-10-1983', salary: '1200.00', discount: '92.32',
    address_id: 4 },
  { name: 'Patrícia Rocha', cpf: '212.044.430-76', born_at: '19-08-1994', salary: '1200.00', discount: '92.32',
    address_id: 5 },
  { name: 'Letícia Sena', cpf: '238.350.890-05', born_at: '26-07-1990', salary: '1200.00', discount: '92.32',
    address_id: 6 },
  { name: 'José Braga', cpf: '642.547.970-10', born_at: '31-05-1969', salary: '1200.00', discount: '92.32',
    address_id: 7 },
  { name: 'Paulo Pereira', cpf: '627.256.670-03', born_at: '27-01-1972', salary: '1200.00', discount: '92.32',
    address_id: 8 },
  { name: 'Luana Prado', cpf: '673.110.360-89', born_at: '03-02-1995', salary: '1200.00', discount: '92.32',
    address_id: 9 },
  { name: 'Camila Freitas', cpf: '230.552.980-55', born_at: '07-08-2002', salary: '1200.00', discount: '92.32',
    address_id: 10 }
]

proponents.each do |proponent|
  Proponent.create!(proponent)
end
