require 'rails_helper'

RSpec.describe 'addresses/show', type: :view do
  before(:each) do
    @address = assign(:address, Address.create!(
                                  street: 'Street',
                                  number: 'Number',
                                  district: 'District',
                                  city: 'City',
                                  state: 'State',
                                  cep: 'Cep'
                                ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Street/)
    expect(rendered).to match(/Number/)
    expect(rendered).to match(/District/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/Cep/)
  end
end
