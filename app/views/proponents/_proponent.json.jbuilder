json.extract! proponent, :id, :name, :cpf, :born_at, :address_id, :salary, :created_at, :updated_at
json.url proponent_url(proponent, format: :json)
